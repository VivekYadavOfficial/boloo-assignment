from django.conf.urls import url
from .views import SellerRegister, SellerLogin, SellerShipmentList, SellerShipmentSync
from drf_yasg.views import get_schema_view
from drf_yasg import openapi
from rest_framework import permissions

schema_view = get_schema_view(
   openapi.Info(
      title="Boloo API",
      default_version='v1',
      description="Test description",
      terms_of_service="https://www.google.com/policies/terms/",
      contact=openapi.Contact(email="contact@snippets.local"),
      license=openapi.License(name="BSD License"),
   ),
   public=True,
   permission_classes=(permissions.AllowAny,),
)

urlpatterns = [
    url(r'^swagger/$', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
    url(r'^register/$', SellerRegister.as_view(), name='Seller Register'),
    url(r'^login/$', SellerLogin.as_view(), name='Seller Login'),
    url(r'^shipmentlist/$', SellerShipmentList.as_view(), name='Shipment List'),
    url(r'^shipmentsync/$', SellerShipmentSync.as_view(), name='Shipment Sync'),
    # url(r'^testing/$', Testing.as_view(), name='Testing'),
]