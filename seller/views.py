from django.shortcuts import render
from rest_framework.views import APIView
from .models import SellerShipment, Orders, SellerAccount
from rest_framework.response import Response
from rest_framework.status import HTTP_200_OK, HTTP_400_BAD_REQUEST
from .tasks import shipment_sync_function
import json
from rest_framework.authtoken.views import obtain_auth_token

# Create your views here.
class SellerRegister(APIView):

    def post(self, request):
        data = request.data
        seller = SellerAccount(seller_name=data['seller_name'], seller_shop_name=data['seller_shop_name'],
                               seller_tin=data['seller_tin'], seller_status=data['seller_status'],
                               seller_client=data['seller_client'], seller_secret=data['seller_secret'])
        SellerAccount.objects.create(seller)
        return Response(data = {"result":"User created successfully"}, status=HTTP_200_OK, content_type='application/json')


class SellerLogin(APIView):

    def post(self, request):
        pass


class SellerShipmentList(APIView):

    def get(self, request):
        """
        GET seller shipment list
        :param request:
        :return:
        """
        shipment_list = dict()
        order_list = dict()
        if str(request.GET.get('type')) == 'shipments':
            shipment_list['shipments'] = SellerShipment.objects.all().values()[:]
            return Response(data=shipment_list, status=HTTP_200_OK, content_type='application/json')
        elif request.GET.get('type') == 'orders':
            order_list['orders'] = Orders.objects.all().values()[:]
            return Response(data=order_list, status=HTTP_200_OK, content_type='application/json')


class SellerShipmentSync(APIView):
    def get(self, request):
        """
        GET method not allowed on this endpoint
        :param request:
        :return:
        """
        return Response(data={"result": "GET not allowed"}, status=HTTP_400_BAD_REQUEST,
                        content_type="application/json")

    def post(self, request):
        """
        In the request body, send client_id and client_secret
        This API asynchrounously creates a task using RabbitMQ to be executed by Celery
        :param request:
        :return:
        """
        data = request.data
        shipment_sync_function.delay(data['client_id'], data['client_secret'])
        return Response(data={
            "result": "Synchronization successfully started. If something wrong, please start sync again"},
            status=HTTP_200_OK, content_type='application/json')
