from django.db import models


# Create your models here.
class CustomerDetails(models.Model):
    customer_id = models.BigAutoField(primary_key= True)
    salutation_code = models.CharField(max_length=250)
    first_name = models.CharField(max_length=250)
    surname = models.CharField(max_length=250)
    street_name = models.CharField(max_length=250)
    house_number = models.CharField(max_length=250)
    house_number_extended = models.CharField(max_length=250)
    address_suppliment = models.CharField(max_length=250)
    extra_address_info = models.CharField(max_length=250)
    zipcode = models.CharField(max_length=250)
    city = models.CharField(max_length=250)
    country_code = models.CharField(max_length=250)
    email = models.CharField(max_length=250)
    company = models.CharField(max_length=250)
    vat_number = models.CharField(max_length=250)
    chamber_of_commerce_number = models.CharField(max_length=250)
    order_reference = models.CharField(max_length=250)
    delivery_phone_number = models.CharField(max_length=250)


class Orders(models.Model):
    order_id = models.CharField(max_length=250)
    order_place_date = models.DateTimeField()


class OrderItems(models.Model):
    OFFER_CONDITION_CHOICES = (
        ('NEW', 'NEW'),
        ('AS_NEW', 'AS_NEW'),
        ('GOOD', 'GOOD'),
        ('REASONABLE', 'REASONABLE'),
        ('MODERATE', 'MODERATE'),
    )
    FULFILMENT_CHOICES = (
        ('FBR', 'FBR'),
        ('FBB', 'FBB'),
    )
    order_id = models.ForeignKey(Orders, on_delete=models.CASCADE)
    order_item_id = models.CharField(max_length=250)
    order_date = models.DateTimeField()
    last_delivery_date = models.DateTimeField()
    ean = models.CharField(max_length=250)
    title = models.CharField(max_length=250)
    quantity = models.IntegerField()
    offer_price = models.IntegerField()
    offer_condition = models.CharField(max_length=250,choices=OFFER_CONDITION_CHOICES)
    offer_reference = models.CharField(max_length=250)
    fulfilment_method = models.CharField(max_length=250,choices=FULFILMENT_CHOICES)


class ShipmentItems(models.Model):
    FULFILMENT_CHOICES = (
        ('FBR', 'FBR'),
        ('FBB', 'FBB'),
    )
    order_item_id = models.CharField(max_length=250)
    order_id = models.CharField(max_length=250)
    fulfilment_method = models.CharField(max_length=250, choices=FULFILMENT_CHOICES)


class Transport(models.Model):
    transport_id = models.IntegerField(primary_key=True)
    transporter_code = models.CharField(max_length=250)
    track_and_trace = models.CharField(max_length=250)
    shipping_label_id = models.CharField(max_length=250)
    shipping_label_code = models.CharField(max_length=250)


class SellerAccount(models.Model):
    SELLER_STATUS = (
        ('ACTIVE', 'ACTIVE'),
        ('DISABLED', 'DISABLED'),
        ('DELETED', 'DELETED'),
        ('BANNED', 'BANNED'),
    )
    seller_id = models.BigAutoField(primary_key=True)
    seller_name = models.CharField(max_length=250)
    seller_shop_name = models.CharField(max_length=250)
    seller_tin = models.CharField(max_length=250)
    seller_status = models.CharField(max_length=10, choices=SELLER_STATUS)
    seller_client = models.CharField(max_length=250)
    seller_secret = models.CharField(max_length=250)

    def __str__(self):
        return self.seller_name

    def get_instance(self):
        return self

    class Meta:
        # ordering = ('-added_on',)
        verbose_name_plural = "Sellers"


class SellerShipment(models.Model):
    # SHIPMENT_STATUS = (
    #     ('ACTIVE', 'ACTIVE'),
    #     ('DISABLED', 'DISABLED'),
    #     ('DELETED', 'DELETED'),
    #     ('BANNED', 'BANNED'),
    # )
    # seller_id = models.ForeignKey(SellerAccount, on_delete=models.CASCADE)
    shipment_id = models.IntegerField(primary_key=True)
    shipment_date = models.DateTimeField()
    shipment_reference = models.CharField(max_length=250)
    # shipment_items = models.ForeignKey(ShipmentItems, on_delete=models.CASCADE)
    # transport_id = models.ForeignKey(Transport, on_delete=models.CASCADE)
    transport_id = models.CharField(max_length=250)
    # shipment_status = models.CharField(max_length=250,choices=SHIPMENT_STATUS)

    def __str__(self):
        return self.shipment_id

    def get_instance(self):
        return self

    # class Meta:
    #     ordering = ('-shipment_date',)
    #     verbose_name_plural = "Shipment Details"
