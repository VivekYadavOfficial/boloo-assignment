from __future__ import absolute_import
from celery import shared_task
from django.core.cache import cache
import requests
from .models import SellerShipment, ShipmentItems


def get_or_refresh_token(client_id, client_secret):
    data = dict()
    data['client_id'] = client_id
    data['client_secret'] = client_secret
    response = requests.post('https://login.bol.com/token?grant_type=client_credentials', data=data).json()
    token = response['access_token']
    timeout = response['expires_in']
    cache.set(client_id + client_secret, token, timeout - 1)
    return token


@shared_task  # Use this decorator to make this a asyncronous function
def shipment_sync_function(client_id, client_secret):
    token = cache.get(client_id + client_secret)
    if not token:
        token = get_or_refresh_token(client_id, client_secret)
    headers = {
        "Authorization": "Bearer " + token,
        "Content-Type": "application/json",
        "Accept": "application/vnd.retailer.v3+json"
    }
    # response = dict()
    # response['status'] = HTTP_200_OK
    # response['token'] = token
    page = 0
    db_shipment_id_fbb = ShipmentItems.objects.filter(fulfilment_method='FBB').first()
    while page < 100:
        page += 1
        response = requests.get("https://api.bol.com/retailer/shipments",
                                params={"page": 1, "fulfilment-method": "FBB"}, headers=headers).json()
        if not response:
            break
        shipments = response['shipments']

        shipment_list = []
        for record in shipments:
            shipment_items = []
            shipment_id = record['shipmentId']
            if shipment_id != db_shipment_id_fbb:
                shipment_date = record['shipmentDate']
                if 'shipmentReference' in record:
                    shipment_reference = record['shipmentReference']
                else:
                    shipment_reference = ""
                transport_id = record['transport']['transportId']
                item_list = record['shipmentItems']
                for item in item_list:
                    shipment_items.append(
                        ShipmentItems(order_item_id=item['orderItemId'], order_id=item['orderId']))
                ShipmentItems.objects.bulk_create(shipment_items)
            else:
                break
            shipment_list.append(SellerShipment(shipment_id=shipment_id, shipment_date=shipment_date,
                                                shipment_reference=shipment_reference, transport_id=transport_id))
        SellerShipment.objects.bulk_create(shipment_list)
    page = 0
    db_shipment_id_fbr = ShipmentItems.objects.filter(fulfilment_method='FBR').first()
    while page < 100:
        page += 1
        response = requests.get('https://api.bol.com/retailer/shipments',
                                params={"page": page, "fulfilment-method": 'FBR'}, headers = headers).json()
        if not response:
            break
        shipments = response['shipments']
        shipment_list = []
        for record in shipments:
            shipment_items = []
            shipment_id = record['shipmentId']
            if shipment_id != db_shipment_id_fbr:
                shipment_date = record['shipmentDate']
                if 'shipmentReference' in record:
                    shipment_reference = record['shipmentReference']
                else:
                    shipment_reference = ""
                transport_id = record['transport']['transportId']
                item_list = record['shipmentItems']
                for item in item_list:
                    shipment_items.append(
                        ShipmentItems(order_item_id=item['orderItemId'], order_id=item['orderId']))
                ShipmentItems.objects.bulk_create(shipment_items)
            else:
                break
            shipment_list.append(SellerShipment(shipment_id=shipment_id, shipment_date=shipment_date,
                                                shipment_reference=shipment_reference, transport_id=transport_id))
        SellerShipment.objects.bulk_create(shipment_list)
