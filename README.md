API Details

POST /seller/register  
	Seller can register using this API by providing their detail  
GET /seller/shipmentlist   
	seller can latest synced shipments in their account (both FBB and FBR)  
POST /seller/shipmentsync  
	API endpoint that can start asychronously fetching and saving of shipment lists to db  